package Dao;

	import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Modelo.ModeloUsuario;


	public class DaoUsuario {
		
		 // Configura essas vari‡veis de acordo com o seu banco
		
	    private final String URL = "jdbc:mysql://localhost/bd_Financas",
	                         USER = "root", 
	                         PASSWORD = "root";

	    private Connection con;
	    private Statement comando;
		
		private void conectar() {
	        try {
	           con = ConFactory.conexao(URL, USER, PASSWORD, ConFactory.MYSQL);
	           comando = con.createStatement();
	           System.out.println("Conectado!");
	        } catch (ClassNotFoundException e) {
	           imprimeErro("Erro ao carregar o driver", e.getMessage());
	        } catch (SQLException e) {
	           imprimeErro("Erro ao conectar", e.getMessage());
	        }
	     }

	     private void fechar() {
	        try {
	           comando.close();
	           con.close();
	           System.out.println("Conex�o Fechada");
	        } catch (SQLException e) {
	           imprimeErro("Erro ao fechar conex�o", e.getMessage());
	        }
	     }

	     private void imprimeErro(String msg, String msgErro) {
	        JOptionPane.showMessageDialog(null, msg, "Erro cr�tico", 0);
	        System.err.println(msg);
	        System.out.println(msgErro);
	        System.exit(0);
	     }
	 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
	     public void insere(ModeloUsuario u){
	         conectar();
	         PreparedStatement insertUsuario = null;
	         
	         try {
	        
	        	 
	        	 String sql ="INSERT INTO usuario VALUES(?,?,?,?)";
	        	 insertUsuario = con.prepareStatement(sql);
	        	
	        	 insertUsuario.setString(1, u.getEmail());
	        	 insertUsuario.setString(2, u.getNome_usuario());
	        	 insertUsuario.setFloat(3, u.getSalario_usuario());
	        	 insertUsuario.setString(4, u.getCategoria_membro());
	        	 
	        	 int r = insertUsuario.executeUpdate();
	        	 
	        	 if (r>0){
	        		 //comando.executeUpdate(sql);
	        		 System.out.println("Inserido!");
	        		 
	        	 }
	           
	        	 
	         } catch (SQLException e) {
	            imprimeErro("Erro ao inserir", e.getMessage());
	         } finally {
	            fechar();
	         }
	      }
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
	    
	     public void altera (ModeloUsuario u){
	    	 conectar();
	    	 
	    	 PreparedStatement alterarUsuario = null;
	    	 
	    	 try{
	    		 
	    	
	    	 String sql = "UPDATE USUARIO" 
	    			 + "SET NOME_USUARIO=?, CATEGORIA_MEMBRO=?,SALARIO_USUARIO=?"
	    			 + "WHERE EMAIL=?";
	    	 
	    	
	    	 alterarUsuario = con.prepareStatement(sql);
	    	 alterarUsuario .setString(1, u.getEmail());
	    	 alterarUsuario .setString(2, u.getNome_usuario());
	    	 alterarUsuario .setFloat(3, u.getSalario_usuario());
	    	 alterarUsuario .setString(4, u.getCategoria_membro());
	    	 
	    	  
	    	int r = alterarUsuario 
	    			.executeUpdate();
	    	
	    	if (r > 0) {
	    		
	    		//comando.executeUpdate(sql);
	    	
	    		System.out.println("Alterado");
	    	}
	    	
	    	 }catch (SQLException e){
	    		
	    		imprimeErro ("Erro ao alterar Usuario" , e.getMessage());
	   	
	    	}
	    	 
	    	 finally {
	         fechar();
	      }
	     }
	     
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	    	
	     // m�todo para listar todos as linhas 
	    	 
	    	 public ArrayList <ModeloUsuario> listaTodos(){
	    		 conectar();
	    		 
	    		 ArrayList <ModeloUsuario> listaModeloUsuario = new ArrayList <ModeloUsuario> ();
	    		 
	    		 try {
	    			 
	    		 
	    		 PreparedStatement selecionarUsuario = null;
	    		 ResultSet rs = null; //vai receber as informa��es que ser�o capitadas do banco de dados 
	    		
	    		 //SELECT seleciona, com o * seleciona tudo e manda para o java ap�s pegar do banco de ddados
	    		 
	    		 String sql = "SELECT * FROM  usuario";
	        			
	    		 selecionarUsuario = con.prepareStatement(sql);
	    		 rs = selecionarUsuario.executeQuery(sql); // retorna um resultado para o usu�rio 
	    		 
	    		 //enquanto ouver informa��es o while vai continuar listando as informa��es no arraylist
	    		 while (rs.next()){
	    			 
	    			 ModeloUsuario u = new ModeloUsuario();
	    			 u.setEmail(rs.getString("email"));//pode colocar n�mero mas n�o � muito viavel, porque ao inserir uma coluna pode dar um erro 
	    			 u.setNome_usuario(rs.getString("nome_usuario"));
	    			 u.setSalario_usuario(rs.getFloat("salario_usuario"));
	    			 u.setCategoria_membro(rs.getString("categoria_membro"));
	    			 
	    			listaModeloUsuario.add(u);
	    			
	    			 
	    		 }
	    		 
	    		 }catch (SQLException e) {
	    			 imprimeErro ("Erro ao selecionar" , e.getMessage());
	    		 }
	    		 return listaModeloUsuario;
	    		   	 
	}
	    	 public ArrayList <ModeloUsuario> listaEmail(String text){
	    		 conectar();
	    		 
	    		 ArrayList <ModeloUsuario> listaModeloUsuario = new ArrayList <ModeloUsuario> ();
	    		 
	    		 try {
	    			 
	    		 
	    		 PreparedStatement selecionarModeloUsuario = null;
	    		 ResultSet rs = null; //vai receber as informa��es que ser�o capitadas do banco de dados 
	    		
	    		 //SELECT seleciona, com o * seleciona tudo e manda para o java ap�s pegar do banco de ddados
	    		 
	    		 String sql = "SELECT * FROM  USUARIO WHERE " + " EMAIL LIKE'" + text + "%'"; 
	    		 		 //% � igual a qualquer coisa, ou seja divide o valor por qualquer coisa 
	        			
	    		 selecionarModeloUsuario = con.prepareStatement(sql);
	    		 rs = selecionarModeloUsuario.executeQuery(sql); // retorna um resultado para o usu�rio 
	    		 
	    		 //enquanto ouver informa��es o while vai continuar listando as informa��es no arraylist
	    		 while (rs.next()){
	    			 
	    			 ModeloUsuario u = new ModeloUsuario();
	    			 u.setEmail(rs.getString("email"));//pode colocar n�mero mas n�o � muito viavel, porque ao inserir uma coluna pode dar um erro 
	    			 u.setNome_usuario(rs.getString("nome_usuario"));
	    			 u.setSalario_usuario(rs.getFloat("salario_usuario"));
	    			 u.setCategoria_membro(rs.getString("categoria_membro"));
	    			 
	    			listaModeloUsuario.add(u);
	    			
	    			 
	    		 }
	    		 
	    		 }catch (SQLException e) {
	    			 imprimeErro ("Erro ao selecionar" , e.getMessage());
	    		 }
	    		 return listaModeloUsuario;
	    	 
	    	 
	    	 
	}
	}

