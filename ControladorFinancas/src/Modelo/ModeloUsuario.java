package Modelo;

public class ModeloUsuario {

	private String email;
	private String nome_usuario;
	private float salario_usuario;
	private String categoria_membro;
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNome_usuario() {
		return nome_usuario;
	}
	public void setNome_usuario(String nome_usuario) {
		this.nome_usuario = nome_usuario;
	}
	public float getSalario_usuario() {
		return salario_usuario;
	}
	public void setSalario_usuario(float salario_usuario) {
		this.salario_usuario = salario_usuario;
	}
	public String getCategoria_membro() {
		return categoria_membro;
	}
	public void setCategoria_membro(String categoria_membro) {
		this.categoria_membro = categoria_membro;
	}
	
	
	
	
	
	
}

