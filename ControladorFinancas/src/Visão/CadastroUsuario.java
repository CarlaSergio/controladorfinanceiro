package Vis�o;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import Dao.DaoUsuario;
import Modelo.ModeloUsuario;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastroUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtNomeU;
	private JTextField txtEmail;
	private JTextField txtSalario;
	private JTextField txtCategoria;
	private JButton btnCadastrar;
	private String teste;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroUsuario frame = new CadastroUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroUsuario() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomeUsuario = new JLabel("Nome Usuario:");
		lblNomeUsuario.setBounds(10, 11, 77, 14);
		contentPane.add(lblNomeUsuario);
		
		txtNomeU = new JTextField();
		txtNomeU.setBounds(97, 8, 293, 17);
		contentPane.add(txtNomeU);
		txtNomeU.setColumns(10);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(10, 36, 46, 14);
		contentPane.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(95, 33, 295, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblSalrio = new JLabel("Sal\u00E1rio:");
		lblSalrio.setBounds(10, 71, 46, 14);
		contentPane.add(lblSalrio);
		
		txtSalario = new JTextField();
		txtSalario.setBounds(95, 64, 295, 17);
		contentPane.add(txtSalario);
		txtSalario.setColumns(10);
		
		JLabel lblCategoria = new JLabel("Categoria:");
		lblCategoria.setBounds(10, 96, 77, 14);
		contentPane.add(lblCategoria);
		
		txtCategoria = new JTextField();
		txtCategoria.setBounds(95, 89, 295, 20);
		contentPane.add(txtCategoria);
		txtCategoria.setColumns(10);
		
		btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				///////////////////////////////
				
				
				//Recupera da interface Gr�fica
				
				String email = txtEmail.getText();
				String nome_usuario = txtNomeU.getText();
				float salario = Float.parseFloat(txtSalario.getText());
				String categoria = txtCategoria.getText();			
				
				ModeloUsuario mu = new ModeloUsuario();
				
				mu.setEmail(email);
				mu.setNome_usuario(nome_usuario);
				mu.setSalario_usuario(salario);
				mu.setCategoria_membro(categoria);
				
				DaoUsuario gravar = new DaoUsuario();
				
				gravar.insere(mu);
				
				JOptionPane.showMessageDialog(null, "Gravado");
				

				////////////////////////////////
			}
		});
		btnCadastrar.setBounds(157, 157, 113, 32);
		contentPane.add(btnCadastrar);
	}
}
