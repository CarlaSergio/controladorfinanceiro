package Vis�o;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JButton;

import Dao.DaoGrupo;
import Dao.DaoUsuario;
import Modelo.ModeloGrupo;
import Modelo.ModeloUsuario;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastroGrupo extends JFrame {

	private JPanel contentPane;
	private JTextField txtNomeGrupo;
	private JTextField txtQtdMembro;
	private JTextField txtMetaAnual;
	private JButton btnCadastrar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroGrupo frame = new CadastroGrupo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroGrupo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomeDoGrupo = new JLabel("Nome do Grupo:");
		lblNomeDoGrupo.setBounds(10, 11, 89, 14);
		contentPane.add(lblNomeDoGrupo);
		
		txtNomeGrupo = new JTextField();
		txtNomeGrupo.setBounds(144, 8, 130, 20);
		contentPane.add(txtNomeGrupo);
		txtNomeGrupo.setColumns(10);
		
		JLabel lblQuantidadeDeMembros = new JLabel("Quantidade de Membros:");
		lblQuantidadeDeMembros.setBounds(10, 51, 130, 14);
		contentPane.add(lblQuantidadeDeMembros);
		
		txtQtdMembro = new JTextField();
		txtQtdMembro.setBounds(143, 48, 131, 20);
		contentPane.add(txtQtdMembro);
		txtQtdMembro.setColumns(10);
		
		JLabel lblMetaAnual = new JLabel("Meta Anual:");
		lblMetaAnual.setBounds(10, 97, 72, 14);
		contentPane.add(lblMetaAnual);
		
		txtMetaAnual = new JTextField();
		txtMetaAnual.setBounds(144, 94, 130, 20);
		contentPane.add(txtMetaAnual);
		txtMetaAnual.setColumns(10);
		
		btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String nome_grupo = txtNomeGrupo.getText();
				int qtd =  Integer.parseInt(txtQtdMembro.getText());
				float meta_anual = Float.parseFloat(txtMetaAnual.getText());
				
				
				ModeloGrupo mg = new ModeloGrupo();
				
				mg.setNome_grupo(nome_grupo);
				mg.setQuantidade_membro(qtd);
				mg.setMeta_anual(meta_anual);
				
				DaoGrupo gravar = new DaoGrupo();
				
				gravar.insere(mg);
				
				JOptionPane.showMessageDialog(null, "Cadastrado");

				
			}
		});
		btnCadastrar.setBounds(144, 160, 130, 23);
		contentPane.add(btnCadastrar);
	}
}
