package Dao;

	import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Modelo.ModeloGrupo;


	public class DaoGrupo {
		
		 // Configura essas vari‡veis de acordo com o seu banco
		
	    private final String URL = "jdbc:mysql://localhost/bd_Financas",
	                         USER = "root", 
	                         PASSWORD = "root";

	    private Connection con;
	    private Statement comando;
		
		private void conectar() {
	        try {
	           con = ConFactory.conexao(URL, USER, PASSWORD, ConFactory.MYSQL);
	           comando = con.createStatement();
	           System.out.println("Conectado!");
	        } catch (ClassNotFoundException e) {
	           imprimeErro("Erro ao carregar o driver", e.getMessage());
	        } catch (SQLException e) {
	           imprimeErro("Erro ao conectar", e.getMessage());
	        }
	     }

	     private void fechar() {
	        try {
	           comando.close();
	           con.close();
	           System.out.println("Conex�o Fechada");
	        } catch (SQLException e) {
	           imprimeErro("Erro ao fechar conex�o", e.getMessage());
	        }
	     }

	     private void imprimeErro(String msg, String msgErro) {
	        JOptionPane.showMessageDialog(null, msg, "Erro cr�tico", 0);
	        System.err.println(msg);
	        System.out.println(msgErro);
	        System.exit(0);
	     }
	 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
	     public void insere(ModeloGrupo g){
	         conectar();
	         PreparedStatement insertGrupo = null;
	         
	         try {
	        
	        	 
	        	 String sql ="INSERT INTO grupo VALUES(?,?,?)";
	        	 insertGrupo = con.prepareStatement(sql);
	        	
	        	 insertGrupo.setString(1, g.getNome_grupo());
	        	 insertGrupo.setInt(2, g.getQuantidade_membro());
	        	 insertGrupo.setFloat(3, g.getMeta_anual());
	        	 
	        	 
	        	 int r = insertGrupo.executeUpdate();
	        	 
	        	 if (r>0){
	        		 //comando.executeUpdate(sql);
	        		 System.out.println("Inserido!");
	        		 
	        	 }
	           
	        	 
	         } catch (SQLException e) {
	            imprimeErro("Erro ao inserir", e.getMessage());
	         } finally {
	            fechar();
	         }
	      }
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
	    
	     public void altera (ModeloGrupo g){
	    	 conectar();
	    	 
	    	 PreparedStatement alterarGrupo = null;
	    	 
	    	 try{
	    		 
	    	
	    	 String sql = "UPDATE USUARIO" 
	    			 + "SET NOME_USUARIO=?, CATEGORIA_MEMBRO=?,SALARIO_USUARIO=?"
	    			 + "WHERE EMAIL=?";
	    	 
	    	
	    	 alterarGrupo = con.prepareStatement(sql);
	    	 alterarGrupo .setString(1, g.getNome_grupo());
	    	 alterarGrupo .setInt(2, g.getQuantidade_membro());
	    	 alterarGrupo .setFloat(3, g.getMeta_anual());
	    	 
	    	 
	    	  
	    	int r = alterarGrupo
	    			.executeUpdate();
	    	
	    	if (r > 0) {
	    		
	    		//comando.executeUpdate(sql);
	    	
	    		System.out.println("Alterado");
	    	}
	    	
	    	 }catch (SQLException e){
	    		
	    		imprimeErro ("Erro ao alterar Grupo" , e.getMessage());
	   	
	    	}
	    	 
	    	 finally {
	         fechar();
	      }
	     }
	     
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	    	
	     // m�todo para listar todos as linhas 
	    	 
	    	 public ArrayList <ModeloGrupo> listaTodos(){
	    		 conectar();
	    		 
	    		 ArrayList <ModeloGrupo> listaModeloGrupo = new ArrayList <ModeloGrupo> ();
	    		 
	    		 try {
	    			 
	    		 
	    		 PreparedStatement selecionarGrupo = null;
	    		 ResultSet rs = null; //vai receber as informa��es que ser�o capitadas do banco de dados 
	    		
	    		 //SELECT seleciona, com o * seleciona tudo e manda para o java ap�s pegar do banco de ddados
	    		 
	    		 String sql = "SELECT * FROM  grupo";
	        			
	    		 selecionarGrupo = con.prepareStatement(sql);
	    		 rs = selecionarGrupo.executeQuery(sql); // retorna um resultado para o usu�rio 
	    		 
	    		 //enquanto ouver informa��es o while vai continuar listando as informa��es no arraylist
	    		 while (rs.next()){
	    			 
	    			 ModeloGrupo u = new ModeloGrupo();
	    			 u.setNome_grupo(rs.getString("nome grupo"));//pode colocar n�mero mas n�o � muito viavel, porque ao inserir uma coluna pode dar um erro 
	    			 u.setQuantidade_membro(rs.getInt("quantidade membro"));
	    			 u.setMeta_anual(rs.getFloat("meta anual"));
	    		
	    			 
	    			listaModeloGrupo.add(u);
	    			
	    			 
	    		 }
	    		 
	    		 }catch (SQLException e) {
	    			 imprimeErro ("Erro ao selecionar" , e.getMessage());
	    		 }
	    		 return listaModeloGrupo;
	    		   	 
	}
	    	 public ArrayList <ModeloGrupo> listaNome_grupo(String text){
	    		 conectar();
	    		 
	    		 ArrayList <ModeloGrupo> listaModeloGrupo = new ArrayList <ModeloGrupo> ();
	    		 
	    		 try {
	    			 
	    		 
	    		 PreparedStatement selecionarModeloGrupo = null;
	    		 ResultSet rs = null; //vai receber as informa��es que ser�o capitadas do banco de dados 
	    		
	    		 //SELECT seleciona, com o * seleciona tudo e manda para o java ap�s pegar do banco de ddados
	    		 
	    		 String sql = "SELECT * FROM  GRUPO WHERE " + " NOME_GRUPO LIKE'" + text + "%'"; 
	    		 		 //% � igual a qualquer coisa, ou seja divide o valor por qualquer coisa 
	        			
	    		 selecionarModeloGrupo = con.prepareStatement(sql);
	    		 rs = selecionarModeloGrupo.executeQuery(sql); // retorna um resultado para o usu�rio 
	    		 
	    		 //enquanto ouver informa��es o while vai continuar listando as informa��es no arraylist
	    		 while (rs.next()){
	    			 
	    			 ModeloGrupo u = new ModeloGrupo();
	    			 u.setNome_grupo(rs.getString("nome grupo"));//pode colocar n�mero mas n�o � muito viavel, porque ao inserir uma coluna pode dar um erro 
	    			 u.setQuantidade_membro(rs.getInt("quantidade membro"));
	    			 u.setMeta_anual(rs.getFloat("meta anual"));
	    			
	    			listaModeloGrupo.add(u);
	    			
	    			 
	    		 }
	    		 
	    		 }catch (SQLException e) {
	    			 imprimeErro ("Erro ao selecionar" , e.getMessage());
	    		 }
	    		 return listaModeloGrupo;
	    	 
	    	 
	    	 
	}
	}

