package Modelo;

public class ModeloGrupo {

	private String nome_grupo;
	private int quantidade_membro;
	private float meta_anual;
	
	
	
	public String getNome_grupo() {
		return nome_grupo;
	}
	public void setNome_grupo(String nome_grupo) {
		this.nome_grupo = nome_grupo;
	
	}
	public int getQuantidade_membro() {
		return quantidade_membro;
	}
	public void setQuantidade_membro(int quantidade_membro) {
		this.quantidade_membro = quantidade_membro;
	}
	public float getMeta_anual() {
		return meta_anual;
	}
	public void setMeta_anual(float meta_anual) {
		this.meta_anual = meta_anual;
	}
	
}

